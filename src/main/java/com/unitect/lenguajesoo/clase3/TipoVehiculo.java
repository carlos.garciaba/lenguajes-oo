/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitect.lenguajesoo.clase3;

/**
 *
 * @author Alumno
 */
public enum TipoVehiculo {
    CAMIONETA(150,5),
    AUTOMOVIL(250,10),
    MOTOCICLETA(300,15);

    public final int velocidadMaxima;
    public final int acelaracion;
    private TipoVehiculo(int velocidadMaxima,int acelaracion) {
        this.velocidadMaxima=velocidadMaxima;
        this.acelaracion=acelaracion;
    }
    
    
}
