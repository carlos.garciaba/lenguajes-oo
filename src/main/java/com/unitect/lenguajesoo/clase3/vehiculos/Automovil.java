package com.unitect.lenguajesoo.clase3.vehiculos;

import com.unitect.lenguajesoo.clase3.TipoVehiculo;
import com.unitect.lenguajesoo.clase3.Vehiculo;

/**
 *
 * @author Carlos GB
 */
public class Automovil extends Vehiculo{
    
    public Automovil(){
        super(TipoVehiculo.AUTOMOVIL);
    }
    
}
