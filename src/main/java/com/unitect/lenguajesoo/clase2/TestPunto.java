package com.unitect.lenguajesoo.clase2;

public class TestPunto {
    
    public static void main(String[] args) {
        Punto p1=new Punto(0,0);
        Punto p2=new Punto(2,3);
        Punto p3=new Punto();
        p3.setX(-4);
        p3.setY(6);
        
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        
        p1.setX(1);
        p1.setX(1);
        
        p2.setX(5);
        p2.setY(-7);
        
        p3.setX(-4);
        p3.setY(-4);
        
        Punto p4=new Punto(10, 10);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        
    }
    
}
