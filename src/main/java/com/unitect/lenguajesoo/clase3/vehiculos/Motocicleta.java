package com.unitect.lenguajesoo.clase3.vehiculos;

import com.unitect.lenguajesoo.clase3.TipoVehiculo;
import com.unitect.lenguajesoo.clase3.Vehiculo;

/**
 *
 * @author Carlos GB
 */
public class Motocicleta extends Vehiculo{
    
    public Motocicleta(){
        super(TipoVehiculo.MOTOCICLETA);
    }
    
}
