package com.unitect.lenguajesoo.clase3;

/**
 *
 * @author Carlos GB
 */
public abstract class Vehiculo {
    
    private final TipoVehiculo tipo;
    private String marca;
    private String color;
    private String modelo;
    private boolean estatus;
    private int kilometraje;
    
    
    protected Vehiculo(TipoVehiculo tipo){
        this.tipo=tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModelo() {
        return modelo;
    }

    public boolean getEstatus() {
        return estatus;
    }

    public void apagarMotor() {
        estatus=false;
    }
    public void encenderMotor() {
        estatus=true;
    }

    public int getVelocidadMaxima() {
        return tipo.velocidadMaxima;
    }
    public int getKilometraje() {
        return kilometraje;
    }

    public TipoVehiculo getTipo() {
        return tipo;
    }
    
    
}
