#Clases y Objetos


## Declaracion de clase:

```java
/**
public class nombreClase{
    tipo nomatributo1;
    tipo nomAtributo2;

    public nombreClae(){} // constructor

    public tipo nombreMetodo([argumentos]..){
    //algoritmo
    }
}
*/
```

## Ejercicio:

Diseñar un algoritmo que simule un conjunto de vehiculos distintos de
la siguiente forma

####Atributos de vehiculo
* tipo
    * camioneta: mas lento
    * automovil
    * motocicleta: mas rapido
* marca
* color
* vel_inicial
* vel_maxima
* kilometraje
* estatus
* velocida actual

####Cada vehiculo tiene las funciones 
* encender motor : cambia estatus a true
* apagar motor : cambia estatus a false
* acelerar: aumenta velocidad actual sumando velocidad inicial
* frenar: disminuye la velocidad actual restando la velocidad inicial;
* leer kilometraje: envia los kilometros recorridos actualmente; 

####Desarrollo::
Construir cada clase para cada tipo de vehiculo con sus respectivos atributos
y funciones.

1. crear 3 objetos de cada tipo
2. mostrar los valores actuales de los atributos de 5 vehiculos distintos
3. encender y desacelerar los 4 vehiculos anteriores
4. mostrar los valores de sus atributos de los 5 anteriores
5. para usar un vehiculo previamente debe encenderse, si se intenta usar el
el vehiculo apagado mostrar un mensaje de error.



